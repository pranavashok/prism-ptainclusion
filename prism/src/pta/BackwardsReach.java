//==============================================================================
//	
//	Copyright (c) 2002-
//	Authors:
//	* Dave Parker <david.parker@comlab.ox.ac.uk> (University of Oxford)
//	
//------------------------------------------------------------------------------
//	
//	This file is part of PRISM.
//	
//	PRISM is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//	
//	PRISM is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with PRISM; if not, write to the Free Software Foundation,
//	Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//	
//==============================================================================

package pta;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.Map.Entry;

import prism.*;
import explicit.*;

public class BackwardsReach extends PrismComponent
{
	/**
	 * Create a new MDPModelChecker, inherit basic state from parent (unless null).
	 */
	public BackwardsReach(PrismComponent parent) throws PrismException
	{
		super(parent);
	}

	/**
	 * Compute the min/max probability of reaching a target in a PTA, from the initial state,
	 * using the backwards reachability method.
	 * @param pta The PTA
	 * @param targetLocs Target locations
	 * @param targetConstraint Target timing contraint
	 * @param min Min or max probabilities (true=min, false=max)
	 * @throws UnsupportedEncodingException 
	 * @throws FileNotFoundException 
	 */
	public double computeProbabilisticReachability(PTA pta, BitSet targetLocs, boolean min) throws PrismException
	{
		if (min)
			throw new PrismException("Backwards reachability does not yet support minmum probabilities");

		// Build backwards graph
		BackwardsReachabilityGraph graph = buildBackwardsGraph(pta, targetLocs);
		
		// Build corresponding MDP
		mainLog.print("Building MDP... ");
		mainLog.flush();
		MDP mdp = graph.buildMDP(pta);

		mainLog.println(mdp.infoString());
		//mdp.exportToDotFile("mdp.dot", target);
		
		// Run a dfs from initial nodes and figure out which nodes are reachable from 0
		List<Integer> reachFromInit = reachableFromInit(mdp);
		
		// Compute reachability probs on MDP
		MDPModelChecker mc = new MDPModelChecker(this);
		mc.setPrecomp(false);
		ModelCheckerResult res = mc.computeReachProbs(mdp, graph.getTarget(), false);

		exportDiagnosticInfo(pta, graph, reachFromInit, mdp, res, "/home/pranav/Coding/jquery.graphviz.svg/");
		
		// Result is max prob over all initial states
		return Math.max(0.0, Utils.minMaxOverArraySubset(res.soln, graph.getInitialStates(), false));
	}

	/**
	 * Build a backwards reachability graph, from a PTA object.
	 */
	public BackwardsReachabilityGraph buildBackwardsGraph(PTA pta, BitSet targetLocs) throws PrismException
	{
		LocZone lz;
		LinkedList<LocZone> explore; // (Waiting)
		IndexedSet<LocZone> found; // (Visited)
		List<LocZone> states;
		BackwardsReachabilityGraph graph;
		List<List<SymbolicEdge>> edgeIndex;
		int src, dest, count;
		long timer;

		mainLog.print("\nBuilding backwards reachability graph...");
		mainLog.flush();
		ProgressDisplay progress = new ProgressDisplay(mainLog);
		progress.start();
		timer = System.currentTimeMillis();

		// Initialise data structures
		graph = new BackwardsReachabilityGraph(pta);
		found = new IndexedSet<LocZone>();
		explore = new LinkedList<LocZone>();
		graph.states = states = new ArrayList<LocZone>();
		edgeIndex = graph.incomingEdgeIndex(pta);

		// Add target states
		//Have we made sure targetLocs contains atleast one true bit?
		count = 0;
		for (int loc = targetLocs.nextSetBit(0); loc >= 0; loc = targetLocs.nextSetBit(loc + 1)) {
			Zone zTarget = DBM.createTrue(pta);
			zTarget.addConstraints(pta.getInvariantConstraints(loc));
			LocZone lzTarget = new LocZone(loc, zTarget, true); // true - goal is reachable from goal
			found.add(lzTarget);
			explore.add(lzTarget);
			states.add(lzTarget);
			graph.addState(pta.getTransitions(loc));
			graph.addTargetState(count++);
		}

		// Reachability loop
		dest = -1;
		// While there are unexplored symbolic states...
		while (!explore.isEmpty()) {
			// Pick next state to explore
			// NB: States are added in order found so we know index of lz is dest+1
			lz = explore.removeFirst();
			dest++;
			//mainLog.println("Exploring: " + dest + ":" + lz);

			// Time predecessor (same for all incoming edges)
			lz = lz.deepCopy();
			lz.tPre(pta);

			int iTrans = -1;
			if (edgeIndex.get(lz.loc) == null)
				continue;
			
			for (SymbolicEdge ed : edgeIndex.get(lz.loc)) {
				iTrans = ed.t;

				int iEdge = ed.e;
				Edge edge = pta.getTransitions(ed.loc).get(iTrans).getEdges().get(iEdge);

				LocZone lzSrc = lz.deepCopy();
				lzSrc.r = false; // Default
				lzSrc.dPre(edge);
				// If predecessor state is non-empty
				if (lzSrc.zone.isEmpty()) 
					continue;
				
				if (targetLocs.get(lzSrc.loc))
					continue;
				
				if(found.contains(lzSrc)) {
					if(states.get(found.get(lzSrc)).r)
						lzSrc.r = true;
				}
				else {
					if( (lz.r == true && edge.getProbability() == 1))
						lzSrc.r = true;	
				}
				
				// Check inclusion
				boolean incl = false;
				// Probably optimize this by checking only those which have the same location
				for(LocZone lz1: found.toArrayList()) { // For every lz1 which was visited
					if (lz1.loc == lzSrc.loc) {
						if (lz1.r == true && lz1.zone.includes((DBM) lzSrc.zone) ) {
							incl = true;
							break;
						}
					}
				}
				if(incl) continue;
				
				// add state
				if (found.add(lzSrc)) {
					//mainLog.println("Added " + Yset.getIndexOfLastAdd() + ":" + lzSrc);
					explore.add(lzSrc); //Shouldn't we add to explore only if it hasn't been visited already?
					states.add(lzSrc);
					graph.addState(pta.getTransitions(lzSrc.loc));
				}
				// end add state
				
				src = found.getIndexOfLastAdd();
				
				graph.addTransition(src, ed.t, ed.e, dest);

				// For each state that lzTmp intersects lzSrc
				int numStatesSoFar = states.size();
				for (int src2 = 0; src2 < numStatesSoFar; src2++) {
					if (src2 == src)
						continue;
					
					LocZone lzTmp = states.get(src2);
					
					if (lzTmp.loc != lzSrc.loc)
						continue;
					
					if(lzTmp.r && lzSrc.r) // If both are reachable from goal
						continue;
					
					Zone zTmp = lzTmp.zone.deepCopy();
					zTmp.intersect(lzSrc.zone);
					if (!zTmp.isEmpty()) {
						// For all reachability graph edges from lzTmp...
						int iTrans2 = ed.t; //Intersect only same transitions
						
						List<List<Integer>> edges2 = graph.getList(src2).get(iTrans2);
						
						int numEdges2 = edges2.size();
						if (numEdges2 < 2) {
							//mainLog.println("SKIP");
							continue;
						}
						for (int iEdge2 = 0; iEdge2 < numEdges2; iEdge2++) {
							List<Integer> dests2 = edges2.get(iEdge2);
							int numDests2 = dests2.size();
							for (int iDest2 = 0; iDest2 < numDests2; iDest2++) {
								int dest2 = dests2.get(iDest2);
								// Edge (src2, iTrans2, iEdge2, dest2)

								// add state

								LocZone lz3 = new LocZone(lzSrc.loc, zTmp);
								
								if (found.add(lz3)) {
									//mainLog.println("Added2 " + Yset.getIndexOfLastAdd() + ":" + lz3);
									explore.add(lz3);
									states.add(lz3);
									graph.addState(pta.getTransitions(lz3.loc));
								} else {
									//mainLog.println("Reusing " + Yset.getIndexOfLastAdd() + ":" + lz3);
								}
								// end add state
								int src3 = found.getIndexOfLastAdd();
								//mainLog.println("^D += " + src3 + ":" + lz3 + "->" + dest + ":...");
								graph.addTransition(src3, iTrans, iEdge, dest);
								//mainLog.println(src3 + ":" + graph.getList(src3));
								//mainLog.println("^D += " + src3 + ":" + lz3 + "->" + dest2 + ":...");
								graph.addTransition(src3, iTrans2, iEdge2, dest2);
								//mainLog.println(src3 + ":" + graph.getList(src3));
							}
						}
					}
				}
			}
			// Print some progress info occasionally
			if (progress.ready())
				progress.update(found.size());
		}

		// Tidy up progress display
		progress.update(found.size());
		progress.end(" states");

		// Determine which are initial states
		// (NB: assume initial location = 0)
		int numStates = states.size();
		for (int st = 0; st < numStates; st++) {
			if (states.get(st).loc == 0) {
				Zone z = states.get(st).zone.deepCopy();
				z.down();
				if (z.includes(DBM.createZero(pta)))
					graph.addInitialState(st);
			}
		}

		// Reachability complete
		timer = System.currentTimeMillis() - timer;
		mainLog.println("Graph constructed in " + (timer / 1000.0) + " secs.");
		mainLog.print("Graph: " + graph.states.size() + " symbolic states");
		mainLog.println(" (" + graph.getInitialStates().size() + " initial, " + graph.getTarget().cardinality() + " target)");

		return graph;
	}
	
	public void exportDiagnosticInfo(PTA pta, BackwardsReachabilityGraph graph, List<Integer> reachFromInit, MDP mdp, ModelCheckerResult res, String dir) {
		// Print dot files
		try {
			String append = "digraph finite_state_machine {\n"+"rankdir=TB;\n"+"size=\"200,200\"\n"+"node [shape = ellipse];\n";
			PrintWriter writer;
			writer = new PrintWriter(dir+"pta.gv","UTF-8");
			writer.print(append+generateDiagnosticsForPTA(pta)+"\n}");
			writer.close();
			writer = new PrintWriter(dir+"mdp.gv","UTF-8");
			for(int state : graph.getInitialStates()) {
				append += "\"" + graph.states.get(state).toStringForDiag() + ": " + Math.max(0.0, Utils.minMaxOverArraySubset(res.soln, Arrays.asList(state), false)) + "\" [fillcolor = \"#ff0000\", color = \"#ff0000\"]\n";
			}
			int count = -1;
			for (int loc = graph.getTarget().nextSetBit(0); loc >= 0; loc = graph.getTarget().nextSetBit(loc + 1)) {
				count++;
				append += "\"" + graph.states.get(count).toStringForDiag() + ": " + Math.max(0.0, Utils.minMaxOverArraySubset(res.soln, Arrays.asList(count), false)) + "\" [fillcolor = \"#00ff00\", color = \"#00ff00\"]\n";
			}
			writer.print(append+generateDiagnosticsForMDP(graph, mdp, res, reachFromInit)+"\n}");
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String generateDiagnosticsForMDP(BackwardsReachabilityGraph graph, MDP mdp, ModelCheckerResult res, List<Integer> reachFromInit)
	{
		int i, j, n;
		String s = "";
		int[] strat = res.simpleStrat;
		for (i = 0; i < mdp.getNumStates(); i++) {
			n = mdp.getNumChoices(i);
			for (j = 0; j < n; j++) {
				Distribution dist = ((MDPSimple)mdp).getChoice(i, j);
				int f = -1;
				for(Integer k : dist.getSupport()) {
					f++;
					if(i < graph.states.size())
						s += "\"" + graph.states.get(i).toStringForDiag() + ": " + Math.max(0.0, Utils.minMaxOverArraySubset(res.soln, Arrays.asList(i), false));
					else
						s += "\"tau " + i + ": " + Math.max(0.0, Utils.minMaxOverArraySubset(res.soln, Arrays.asList(i), false));
					s += "\" -> ";
					if(k < graph.states.size())
						s += "\"" + graph.states.get(k).toStringForDiag() + ": " + Math.max(0.0, Utils.minMaxOverArraySubset(res.soln, Arrays.asList(k), false));
					else
						s += "\"tau " + k + ": " + Math.max(0.0, Utils.minMaxOverArraySubset(res.soln, Arrays.asList(k), false));
					if(i < graph.states.size())
						s += "\" [label = \"e" + j + "f" + f + "-" + dist.get(k) + "\"";
					else
						s += "\" [label = \"tau\"";
					if(strat[i] == j && reachFromInit.contains(i))
						s += ", color = \"#ff0000\"";
					else
						s += ", color = \"#000000\"";
					s += "];\n";
				}
			}
		}
		return s;
	}
	
	public String generateDiagnosticsForPTA(PTA pta)
	{
		String s = "";
		for(int src = 0; src < pta.numLocations; src++) {
			int tr = -1;
			for(Transition t : pta.getTransitions(src)) {
				tr++;
				for(int e = 0; e < t.getNumEdges(); e++) {
					s += src + " -> " + t.getEdges().get(e).getDestination();
					s += " [label = \"" + Constraint.toStringList(pta, t.getGuardConstraints()) + " # e" + tr + "f" + e + " # " + t.getEdges().get(e).getProbability() + "\" ]\n";  
				}
			}
		}
		return s;		
	}
	
	public List<Integer> reachableFromInit(MDP m) {
		MDPSimple mdp = (MDPSimple) m;
		Stack<Integer> st = new Stack<Integer>();
		List<Integer> explore = new ArrayList<Integer>();
		Iterable<Integer> initialStates = mdp.getInitialStates();
		for(int i : initialStates) {
			st.push(i);
		}
		while(!st.isEmpty()) {
			int cur = st.pop();
			explore.add(cur);
			for(Distribution choice : mdp.getChoices(cur)) {
				for(Integer dest : choice.getSupport()) {
					if(!explore.contains(dest))
						st.push(dest);
				}
			}
		}
		return explore;		
	}
}
