package pta;

import java.util.Arrays;

public class SymbolicEdge
{
	int loc;
	int t; // Transition index
	int e; // Edge index
	
	public SymbolicEdge(int l, int t, int e) {
		this.loc = l;
		this.t = t;
		this.e = e;
	}
	
	@Override
	public String toString() {
		return "(" + loc + ", " + t + ", " + e + ")"; 
	}
	
	public int hashCode() {
		return Arrays.deepHashCode(new Object[] {loc, t, e});
	}
	
	public boolean equals(Object o)
	{
		if(o == null) return false;
		SymbolicEdge ed;
		try {
			ed = (SymbolicEdge) o;
		} catch (ClassCastException e) {
			return false;
		}
		if (loc != ed.loc)
			return false;
		if (t != ed.t)
			return false;
		return (e == ed.e);
	}
}
